import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import { ConfigService } from '../config.service';
import { Todo } from '../interfaces/todo.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent { }

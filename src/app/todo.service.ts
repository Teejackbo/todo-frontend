import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { CreateTodoDTO } from './interfaces/dto/create-todo.dto';
import { Todo } from './interfaces/todo.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private _url: string;
  private _data: Todo[] = [];
  private _todos: Subject<Todo[]> = new Subject();
  $todos = this._todos.asObservable();

  constructor(private http: HttpClient, private _config: ConfigService) {
    this._url = _config.get('baseUrl');
  }

  private _notify() {
    this._todos.next(this._data);
  }

  getTodos() {
    this.http.get(this._url).subscribe((data: Todo[]) => {
      this._data = data;
      this._notify();
    });
  }

  createTodo(todo: CreateTodoDTO) {
    this.http.post(this._url, todo).subscribe((data: Todo) => {
      this._data.push(data);
      this._notify();
    });
  }

  updateTodo(todo: Todo) {
    const { text, completed } = todo;
    this.http.put(`${this._url}/${todo.id}`, { text, completed }).subscribe(data => {
      const index = this._data.indexOf(todo);
      this._data[index] = todo;
    });
  }

  deleteTodo(todo: Todo) {
    this.http.delete(`${this._url}/${todo.id}`).subscribe(data => {
      const index = this._data.indexOf(todo);
      this._data.splice(index, 1);
      this._notify();
    });
  }
}

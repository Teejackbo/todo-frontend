import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Todo } from '../interfaces/todo.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  private _data: Observable<{}>;
  todos: Todo[] = [];

  constructor(private readonly _todoService: TodoService) {
    this._data = _todoService.$todos;
    this._data.subscribe((data: Todo[]) => this.todos = data);
  }

  ngOnInit() {
    this._todoService.getTodos();
  }
}

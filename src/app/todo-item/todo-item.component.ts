import { Component, Input } from '@angular/core';
import { Todo } from '../interfaces/todo.interface';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.sass']
})
export class TodoItemComponent {
  @Input() todo: Todo;
  editing = false;

  constructor(private _todoService: TodoService) { }

  toggleEdit(): void {
    this.editing = !this.editing;
  }

  toggleCompletion(): void {
    this.todo.completed = !this.todo.completed;
    this.updateTodo();
  }

  updateTodo(): void {
    this.editing = false;
    this._todoService.updateTodo(this.todo);
  }

  deleteTodo(): void {
    this._todoService.deleteTodo(this.todo);
  }
}

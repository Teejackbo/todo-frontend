import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import { CreateTodoDTO } from '../interfaces/dto/create-todo.dto';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.sass']
})
export class CreateTodoComponent {
  text: string;
  constructor(private _todoService: TodoService) { }

  submitTodo(): void {
    const todo = new CreateTodoDTO();
    todo.text = this.text;
    todo.completed = false;

    this._todoService.createTodo(todo);
    this.resetText();
  }

  handleEnter(e: KeyboardEvent): void {
    if (e.which === 13) {
      this.submitTodo();
    }
  }

  resetText(): void {
    this.text = '';
  }
}

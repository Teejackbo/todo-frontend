export class CreateTodoDTO {
  text: string;
  completed: boolean;
}

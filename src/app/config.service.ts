import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  config = {
    baseUrl: '//127.0.0.1:3000'
  };

  getConfig() {
    return this.config;
  }

  get(property: string) {
    return this.config[property];
  }
}
